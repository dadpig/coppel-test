//
//  NetworkManager.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation
//https://developers.themoviedb.org
class NetworkConfig {
  static let scheme = "https",
             host = "api.themoviedb.org",
             apiKey = "d494dd9d94edbd853f958ebc880599c3",
             isDebug = true
}

var components = URLComponents()

enum Result<T> {
  case success(T)
  case failure(Error)
}

fileprivate typealias Config = NetworkConfig

class NetworkManger{

  public static let shared = NetworkManger()
  
  private func url(path: EndpointPath, queryItems: [URLQueryItem]?) -> URL? {
      var components = URLComponents()
      components.scheme = Config.scheme
      components.host = Config.host
      components.path = path
      components.queryItems = queryItems ?? []
      return components.url
  }

  var task: URLSessionTask?
  
  public func setParams(_ params: [String: Any]?, in urlRequest: inout URLRequest){
    guard let params = params,
          let data = try? JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted),
          let json = String(data: data, encoding: .utf8),
          let data = json.data(using: .utf8)
    else { return }
    urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
    urlRequest.httpBody = data
  }
  
  public func request<Response: Decodable>(_ endpoint: Endpoint<Response>, completion: @escaping (Result<Response>) -> Void) {
    guard let url = url(path: endpoint.path, queryItems: endpoint.queryItems) else { return }
    let session = URLSession.shared
    var urlRequest = URLRequest(url: url)
    urlRequest.httpMethod = endpoint.method.rawValue
    setParams(endpoint.parameters,in: &urlRequest)
    task = session.dataTask(with: urlRequest){ (data, response, error) in
      if let error = error {
        if NetworkConfig.isDebug {
          print(error)
        }
        completion(.failure(error))
      } else if let data = data {
        print(String(decoding: data, as: UTF8.self))
        do {
          let response = try Response(data: data)
          if NetworkConfig.isDebug {
            print(response)
          }
          completion(.success(response))
        }catch {
          if NetworkConfig.isDebug {
            print(error)
          }
          completion(.failure(error))
        }
      }
    }
    task?.resume()
  }
}
