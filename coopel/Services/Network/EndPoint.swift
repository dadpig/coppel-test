//
//  EndPoint.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation

public enum Api {}

protocol CustomPathComvertible {
  var path: String { get }
}

typealias Parameters = [String: Any]
typealias EndpointPath = String
enum Method: String {
  case get, post, put, patch, delete
}

final public class Endpoint<Response> {
  public typealias EndpointDecode = (Data) throws -> Response

  let method: Method
  let path: EndpointPath
  let queryItems: [URLQueryItem]?
  let authRequestToken: Bool
  let parameters: Parameters?

  init(method: Method = .get,
       path: EndpointPath,
       authRequestToken: Bool = false,
       queryItems: [URLQueryItem]? = nil,
       parameters: Parameters? = nil
       
  ) {
    self.method = method
    self.path = path
    self.authRequestToken = authRequestToken
    self.queryItems = queryItems
    self.parameters = parameters
  }
}
