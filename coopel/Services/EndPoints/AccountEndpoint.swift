//
//  AccountEndpoint.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation

enum AccountEndPoint {
  static let basePath = "/3/account"
  
  enum Path: String, CustomPathComvertible {
    var path: String {
      switch self {
      case .favorite:
        return basePath + self.rawValue
      case .detail:
        return basePath
      }
    }
    
    case favorite = "/{account_id}/favorite/movies"
    case detail = ""
  }
  
  static func detail() -> Endpoint<Account> {
    .init(
      method: .get,
      path: Path.detail.path,
      queryItems: [
        .init(name: "api_key", value: NetworkConfig.apiKey),
        .init(name: "session_id", value: UserDefaults.sessionId ?? "")
      ]
    )
  }
  
  static func favorite(accountId: Int) -> Endpoint<ResponseMoviesList> {
    var path = Path.favorite.path
    path = path.replacingOccurrences(of: "{account_id}", with: "\(accountId)")
    return .init(
      method: .get,
      path: path,
      queryItems: [
        .init(name: "api_key", value: NetworkConfig.apiKey),
        .init(name: "session_id", value: UserDefaults.sessionId ?? "")
      ]
    )
  }
  
  
  
}

