//
//  TVEndpoint.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation

enum TVEndPoint {
  static let basePath = "/3/tv"
  
  enum Path: String, CustomPathComvertible {
    var path: String {
      basePath+self.rawValue
    }
    
    case on = "/on_the_air"
    case air = "/airing_today"
  }
  
  static func onAir(page: Int) -> Endpoint<ResponseTVList> {
    .init(
      method: .get,
      path: Path.on.path,
      queryItems: [
        .init(name: "api_key", value: NetworkConfig.apiKey)
      ]
    )
  }
  
  static func airingToday(page: Int) -> Endpoint<ResponseTVList> {
    .init(
      method: .get,
      path: Path.air.path,
      queryItems: [
        .init(name: "api_key", value: NetworkConfig.apiKey)
      ]
    )
  }
  
}
