//
//  User.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation
////https://developers.themoviedb.org/3/authentication/how-do-i-generate-a-session-id.
enum AuthEndPoint {
  static let path = "/3/authentication"
  
  static func login(user: String, password: String, requestToken: String) -> Endpoint<RequestToken> {
    .init(
      method: .post,
      path: path+"/token/validate_with_login",
      queryItems: [.init(name: "api_key", value: NetworkConfig.apiKey)],
      parameters: [
        "username": user,
        "password": password,
        "request_token": requestToken
      ]
    )
  }
  static func createSession() -> Endpoint<CreateSession> {
    .init(
      method: .post,
      path: path+"/session/new",
      queryItems: [.init(name: "api_key", value: NetworkConfig.apiKey)],
      parameters: [
        "request_token": UserDefaults.loginRequestToken ?? ""
      ]
    )
  }
  static func token() -> Endpoint<RequestToken> {
    .init(
      method: .get,
      path: path+"/token/new",
      queryItems: [.init(name: "api_key", value: NetworkConfig.apiKey)]
    )
  }
}
