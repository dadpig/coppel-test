//
//  Movie.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation

enum MovieEndPoint {
  static let basePath = "/3/movie"
  
  enum Path: String, CustomPathComvertible {
    var path: String {
      basePath+self.rawValue
    }
    
    case popular = "/popular"
    case topRated = "/top_rated"
  }
  
  static func popular(page: Int) -> Endpoint<ResponseMoviesList> {
    .init(
      method: .get,
      path: Path.popular.path,
      queryItems: [
        .init(name: "api_key", value: NetworkConfig.apiKey)
      ]
    )
  }
  
  static func topRated(page: Int) -> Endpoint<ResponseMoviesList> {
    .init(
      method: .get,
      path: Path.topRated.path,
      queryItems: [
        .init(name: "api_key", value: NetworkConfig.apiKey)
      ]
    )
  }
  
}
