//
//  Movies.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation



typealias Movies = [Movie]

struct Movie: Decodable {
  let id: Int
  let originalTitle: String
  let overview: String
  let posterPath: String
  let backdropPath: String
  let releaseDate: String
  let voteAverage: Double
}


struct ResponseMoviesList: Decodable {
  let page: Int
  let results: Movies
}
