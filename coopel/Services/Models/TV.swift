//
//  TV.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation

typealias TVs = [TV]

struct TV: Decodable {
  let id: Int
  let name: String
  let overview: String
  let posterPath: String
  let backdropPath: String?
  let firstAirDate: String
  let voteAverage: Double
}


struct ResponseTVList: Decodable {
  let page: Int
  let results: TVs
}
