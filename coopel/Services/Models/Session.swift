//
//  Session.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation

struct RequestToken: Decodable {
  let success: Bool
  let expiresAt: String
  let requestToken: String
}

struct CreateSession: Decodable {
  let success: Bool
  let sessionId: String
}
