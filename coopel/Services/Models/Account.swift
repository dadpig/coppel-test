//
//  Account.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation

struct Account: Decodable {
  let id: Int
  let name: String
  let username: String
}
