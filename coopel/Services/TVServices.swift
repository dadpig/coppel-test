//
//  TVServices.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation

class TVService: TVServiceContract {
  func onAir(page: Int, completion: @escaping (Result<TVs>) -> Void) {
    NetworkManger.shared.request(TVEndPoint.onAir(page: page)){ result in
      switch result {
      case .success(let response):
        completion(.success(response.results))
      case .failure(let error):
        completion(.failure(error))
      }
    }
  }
  
  func airingToday(page: Int, completion: @escaping (Result<TVs>) -> Void) {
    NetworkManger.shared.request(TVEndPoint.airingToday(page: page)){ result in
      switch result {
      case .success(let response):
        completion(.success(response.results))
      case .failure(let error):
        completion(.failure(error))
      }
    }
  }
}
