//
//  MoviesServices.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation

class MoviesService: MovieServiceContract {
  func popular(page: Int, completion: @escaping (Result<Movies>) -> Void) {
    NetworkManger.shared.request(MovieEndPoint.popular(page: page)){ result in
      switch result {
      case .success(let response):
        completion(.success(response.results))
      case .failure(let error):
        completion(.failure(error))
      }
    }
  }
  
  func topRated(page: Int, completion: @escaping (Result<Movies>) -> Void) {
    NetworkManger.shared.request(MovieEndPoint.topRated(page: page)){ result in
      switch result {
      case .success(let response):
        completion(.success(response.results))
      case .failure(let error):
        completion(.failure(error))
      }
    }
  }
}
