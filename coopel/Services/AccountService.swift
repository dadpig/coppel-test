//
//  AccountService.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation

class AccountService: AccountServiceContract {
  func detail(completion: @escaping (Result<Account>) -> Void) {
    NetworkManger.shared.request(AccountEndPoint.detail(), completion: completion)
  }
  
  func favorites(accountId: Int, completion: @escaping (Result<Movies>) -> Void) {
    NetworkManger.shared.request(AccountEndPoint.favorite(accountId: accountId)){ result in
      switch result {
      case .success(let response):
        completion(.success(response.results))
      case .failure(let error):
        completion(.failure(error))
      }
    }
  }
 
}
