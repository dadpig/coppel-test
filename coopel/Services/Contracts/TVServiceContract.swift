//
//  TVServiceContract.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation

protocol TVServiceContract: AnyObject {
  func onAir(page: Int, completion: @escaping (Result<TVs>) -> Void)
  func airingToday(page: Int, completion: @escaping (Result<TVs>) -> Void)
}
