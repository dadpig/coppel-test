//
//  LoginServiceContract.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation

protocol AuthServiceContract: AnyObject {
  func login(user: String, passwd: String, completion: @escaping (Result<RequestToken>) -> Void)
  static func token(completion: @escaping (Result<RequestToken>) -> Void)
}
