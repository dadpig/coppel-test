//
//  MovieServiceContract.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation


protocol MovieServiceContract: AnyObject {
  func popular(page: Int, completion: @escaping (Result<Movies>) -> Void)
  func topRated(page: Int, completion: @escaping (Result<Movies>) -> Void)
}
