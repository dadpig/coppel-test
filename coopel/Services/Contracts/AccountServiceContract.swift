//
//  AccountServiceContract.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation

protocol AccountServiceContract: AnyObject {
  func detail(completion: @escaping (Result<Account>) -> Void)
  func favorites(accountId: Int, completion: @escaping (Result<Movies>) -> Void)
}
