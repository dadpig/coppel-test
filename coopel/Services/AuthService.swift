//
//  LoginService.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation

class AuthService: AuthServiceContract {
  
  static func createSession(completion: ((Result<CreateSession>) -> Void)?) {
    NetworkManger.shared.request(AuthEndPoint.createSession()){ result in
      if case .success(let response) = result {
        UserDefaults.sessionId = response.sessionId
      }
      completion?(result)
    }
  }
  
  
  func login(user: String, passwd: String, completion: @escaping (Result<RequestToken>) -> Void) {
    Self.token(){ result in
      switch result {
      case .success(let response):
        NetworkManger.shared.request(AuthEndPoint.login(user: user, password: passwd, requestToken: response.requestToken)){ result in
          switch result {
          case .success(let response):
            UserDefaults.loginRequestToken = response.requestToken
            completion(.success(response))
          case .failure(let error):
            completion(.failure(error))
          }
        }
      case .failure(let error):
        completion(.failure(error))
      }
    }
  }
  static func token(completion: @escaping (Result<RequestToken>) -> Void){
    NetworkManger.shared.request(AuthEndPoint.token()){ result in
      if case .success(let response) = result {
        UserDefaults.requestToken = response.requestToken
      }
      completion(result)
    }
  }
}
