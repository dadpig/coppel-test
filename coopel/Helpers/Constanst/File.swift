//
//  File.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation

enum GlobalConstants {
  static let backgroundAddress = "https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/5f297040585033.57851fbd33ae2.jpg",
             bulbasaurBackgroundAddress = "https://wallpaperaccess.com/full/4589134.jpg"
             
  
  enum Texts {
    static let ok = "OK",
    cancel = "cancel",
               errorTitle = "BAD"
  }
}
