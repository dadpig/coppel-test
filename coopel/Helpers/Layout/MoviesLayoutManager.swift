//
//  MoviesLayoutManager.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import UIKit

fileprivate enum Constants {
  static let itemheighth: CGFloat = 248,
             itemMargin: CGFloat = 8
}

class MoviesLayoutManager {
  static func compositionalLayout() -> UICollectionViewCompositionalLayout{
    .init { sectionNumber, env in
      let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(0.5), heightDimension: .absolute(Constants.itemheighth)))
      item.contentInsets = .init(top: Constants.itemMargin, leading: Constants.itemMargin, bottom: Constants.itemMargin, trailing: Constants.itemMargin)
      let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(Constants.itemheighth))
      let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2)
      let section = NSCollectionLayoutSection(group: group)
      return section
    }
  }
}
