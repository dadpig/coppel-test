//
//  UIImageView+Extensin.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation
import UIKit

//https://image.tmdb.org/t/p/w500/kqjL17yufvn9OVLyXYpvtyrFfak.jpg

fileprivate struct ImageLoadConfig {
  static let scheme = "https",
             host = "image.tmdb.org",
             path = "/t/p/w500/"
  
  static var baseUrlComponents: URLComponents {
    var components = URLComponents()
    components.scheme = scheme
    components.host = host
    return components
  }
}

public extension UIImageView {
  fileprivate static let failImage = UIImage(systemName: "exclamationmark.triangle.fill")
  
  fileprivate func setImage(_ image: UIImage?){
    DispatchQueue.main.async { [weak self] in
      self?.image = image
    }
  }
  
  func loadFrom(URLAddress address: String?) {
    loadFrom(url: URL(string: address ?? ""))
  }
  
  func loadFrom(url: URL?) {
    guard let url = url else {
      setImage(Self.failImage)
      return
    }
    DispatchQueue.global().async {
      guard let data = try? Data(contentsOf: url) else {self.setImage(Self.failImage); return}
      self.setImage(UIImage(data: data))
    }
  }
  
  func loadFrom(path: String) {
    var  components = ImageLoadConfig.baseUrlComponents
    components.path = ImageLoadConfig.path + path
    loadFrom(url: components.url)
  }
}
