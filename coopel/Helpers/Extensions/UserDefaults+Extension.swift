//
//  UserDefaults+Extension.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation

extension UserDefaults {
  public enum Keys {
    static let requestToken: String = "request_token"
    static let sessionId: String = "session_id"
    static let loginRequestToken: String = "login_request_token"
    static let favorites: String = "favorites"
  }
  
  @UserDefault(key: Keys.requestToken, defaultValue: nil)
  static var requestToken: String?
  //TODO: move to keyChange
  @UserDefault(key: Keys.loginRequestToken, defaultValue: nil)
  static var loginRequestToken: String?
  //TODO: move to keyChange
  @UserDefault(key: Keys.sessionId, defaultValue: nil)
  static var sessionId: String?
  //TODO: move to storage or best move to serivice for multintegration
  @UserDefault(key: Keys.favorites, defaultValue: [])
  static var favorites: [Int]
  static func isFavorite(_ id: Int) -> Bool {
    favorites.contains(id)
  }
  static func toggleFavorite(_ id: Int){
    var favorites = favorites
    if let index = favorites.firstIndex(of: id) {
      favorites.remove(at: index)
    } else {
      favorites.append(id)
    }
    self.favorites = favorites
  }
}
