//
//  Decode+Extension.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation

extension Decodable {
  init(data: Data, using decoder: JSONDecoder = .init()) throws {
    decoder.keyDecodingStrategy = .convertFromSnakeCase
    self = try decoder.decode(Self.self, from: data)
  }

  init(JSON: [String:Any], using decoder: JSONDecoder = .init()) throws {
    let data = try JSONSerialization.data(withJSONObject: JSON , options: .prettyPrinted)
    try self.init(data: data, using: decoder)
  }

}
