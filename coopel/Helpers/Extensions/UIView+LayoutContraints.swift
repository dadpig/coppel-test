//
//  UIView+LayoutContraints.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import UIKit

extension UIView {
  fileprivate func notMask() {
    translatesAutoresizingMaskIntoConstraints = false
  }
  
  func edges(to view: UIView?, top: CGFloat = 0, left: CGFloat = 0, bottom: CGFloat = 0, right: CGFloat = 0, topAnchor: NSLayoutYAxisAnchor? = nil, bottonAnchor: NSLayoutYAxisAnchor? = nil) {
    guard let view = view else { return }
    notMask()
    
    NSLayoutConstraint.activate([
      self.leftAnchor.constraint(equalTo: view.leftAnchor, constant: left),
      self.rightAnchor.constraint(equalTo: view.rightAnchor, constant: right),
      self.topAnchor.constraint(equalTo: topAnchor ?? view.topAnchor, constant: top),
      self.bottomAnchor.constraint(equalTo: bottonAnchor ?? view.bottomAnchor, constant: bottom)
    ])
  }

  func edges(to guide: UILayoutGuide?, top: CGFloat = 0, left: CGFloat = 0, bottom: CGFloat = 0, right: CGFloat = 0, topAnchor: NSLayoutYAxisAnchor? = nil, bottonAnchor: NSLayoutYAxisAnchor? = nil) {
    guard let guide = guide else { return }
    notMask()
    NSLayoutConstraint.activate([
      self.leftAnchor.constraint(equalTo: guide.leftAnchor, constant: left),
      self.rightAnchor.constraint(equalTo: guide.rightAnchor, constant: right),
      self.topAnchor.constraint(equalTo: topAnchor ?? guide.topAnchor, constant: top),
      self.bottomAnchor.constraint(equalTo: bottonAnchor ?? guide.bottomAnchor, constant: bottom)
    ])
  }

  func center(to view: UIView, centerX: CGFloat = 0, centerY: CGFloat = 0) {
    notMask()
    NSLayoutConstraint.activate([
      self.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: centerX),
      self.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: centerY)
    ])
  }
}

