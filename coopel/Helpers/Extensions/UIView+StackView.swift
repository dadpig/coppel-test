//
//  UIView+StackView.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import UIKit

public extension UIView {
  
  fileprivate func Stack(_ axis: NSLayoutConstraint.Axis, views: [UIView], spacing: CGFloat, alignment: UIStackView.Alignment, distribution: UIStackView.Distribution) -> UIStackView {
    let stackView = UIStackView(arrangedSubviews: views)
    stackView.axis = axis
    stackView.alignment = alignment
    stackView.distribution = distribution
    stackView.spacing = spacing
    stackView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(stackView)
    stackView.edges(to: self)
    return stackView
  }
  
  @discardableResult
  func VStack(views: [UIView], spacing: CGFloat = UIStackView.spacingUseDefault, alignment: UIStackView.Alignment = .fill, distribution: UIStackView.Distribution = .fill) -> UIStackView {
    Stack(.vertical, views: views, spacing: spacing, alignment: alignment, distribution: distribution)
  }
  
  @discardableResult
  func HStack(views: [UIView], spacing: CGFloat = UIStackView.spacingUseDefault, alignment: UIStackView.Alignment = .fill, distribution: UIStackView.Distribution = .fill) -> UIStackView {
    Stack(.horizontal, views: views, spacing: spacing, alignment: alignment, distribution: distribution)
  }
}
