//
//  HomeViewController.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import UIKit

fileprivate enum HomeConstants {
  static let alertProfile = "Profile",
             alertLogOut = "Log Out",
             what = "What?",
             title = "TV Shows",
             segmentedHeight: CGFloat = 44
}



fileprivate typealias Constants = HomeConstants
class HomeViewController: UIViewController {
  typealias Item = HomePresenter.ListItem
  
  fileprivate var movies = [Item: [MovieCellModel]]()
  fileprivate lazy var presenter = HomePresenter(movieService: MoviesService(), tvService: TVService(), view: self)
  private var collectionViews = [Item: UICollectionView]()
  private var visibleCollectionView: UICollectionView? {
    guard let item = visibleItem, collectionViews.keys.contains(item)else {return nil}
    return collectionViews[item]
  }
  private var visibleItem: Item?
  fileprivate lazy var backgroundView: UIImageView = {
    let view = UIImageView()
    self.view.addSubview(view)
    return view
  }()
  fileprivate lazy var segmentedControl: UISegmentedControl = {
    let segment = UISegmentedControl(items: presenter.segmentitems.map{ $0.rawValue})
    self.view.addSubview(segment)
    segment.addTarget(self, action: #selector(segmentAction), for: .valueChanged)
    segment.selectedSegmentIndex = 0
    segment.translatesAutoresizingMaskIntoConstraints = false
    return segment
  }()
  fileprivate lazy var container: UIView = {
    let view = UIView()
    self.view.addSubview(view)
    return view
  }()
  
  private func hideVisibleCollectionviewIfnedded(){
    guard let collectionView = visibleCollectionView else { return }
    container.sendSubviewToBack(collectionView)
    collectionView.alpha = 0
  }
  
  private func bringCollectionView(at index: Int) {
    let collectionview = collectionView(at: index)
    hideVisibleCollectionviewIfnedded()
    container.bringSubviewToFront(collectionview)
    collectionview.alpha = 1
    visibleItem = presenter.item(at: index)
  }
  
  private func collectionView(at index: Int) -> UICollectionView {
    let item = presenter.item(at: index)
    if let collection = collectionViews[item]{
      return collection
    }
    let collection = UICollectionView(frame: .zero, collectionViewLayout: MoviesLayoutManager.compositionalLayout())
    collection.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: String(describing: MovieCollectionViewCell.self))
    collection.delegate = self
    collection.dataSource = self
    collection.backgroundColor = .clear
    container.addSubview(collection)
    collection.edges(to: container)
    collection.tag = index
    collectionViews[item] = collection
    return collection
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupNav()
    segmentedControl.sendActions(for: .valueChanged)
  }
  
  private func setupUI(){
    let guide = view.safeAreaLayoutGuide
    title = Constants.title
    backgroundView.edges(to: view)
    backgroundView.loadFrom(URLAddress: GlobalConstants.bulbasaurBackgroundAddress)
    NSLayoutConstraint.activate([
      segmentedControl.leftAnchor.constraint(equalTo: guide.leftAnchor),
      segmentedControl.rightAnchor.constraint(equalTo: guide.rightAnchor),
      segmentedControl.topAnchor.constraint(equalTo: guide.topAnchor),
      segmentedControl.heightAnchor.constraint(equalToConstant:  Constants.segmentedHeight)
    ])
    container.edges(to: guide, topAnchor: segmentedControl.bottomAnchor)
  }
  
  private func setupNav(){
    navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(userTapped))
  }
  
  @objc func userTapped(sender: Any?){
    let alert = UIAlertController(title: Constants.what, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: Constants.alertProfile, style: .default, handler: handlerAlert))
        alert.addAction(UIAlertAction(title: Constants.alertLogOut, style: .destructive, handler: handlerAlert))
        alert.addAction(UIAlertAction(title: GlobalConstants.Texts.cancel, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
  }
  
  @objc func segmentAction(_ segmentedControl: UISegmentedControl) {
    let index = segmentedControl.selectedSegmentIndex
    bringCollectionView(at: index)
    presenter.fetchMovies(by: index)
  }
  
  func handlerAlert(_ action: UIAlertAction){
    if action.style == .default {
      presenter.didSelectProfile()
    }else if action.style == .destructive {
      presenter.didSelectlogOut()
    }
  }
  
}


extension HomeViewController: HomeViewable{
  func success(models: [MovieCellModel], at index: Int) {
    let item = presenter.item(at: index)
    self.movies[item] = models
    if let collection = collectionViews[item]{
      DispatchQueue.main.async {
        collection.reloadData()
      }
    }
  }
  
  func showProfile() {
    let vc = ProfileViewController()
    present(vc, animated: true)
  }
  
  func gotoLogin(){
    guard let scene = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else {return}
    DispatchQueue.main.async {
      scene.rootNav?.setViewControllers([LoginViewController()], animated: false)
    }
  }
  
}


extension HomeViewController: UICollectionViewDelegate {
  
}

extension HomeViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard let movies = movies[presenter.item(at: collectionView.tag)] else{return}
    let vc = ItemDetailViewController(presenter: .init(model: movies[indexPath.item]))
    show(vc, sender: nil)
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    guard let movies = movies[presenter.item(at: collectionView.tag)] else { return 0 }
    return movies.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let movies = movies[presenter.item(at: collectionView.tag)],
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MovieCollectionViewCell.self), for: indexPath) as? MovieCollectionViewCell
    else  {
      return .init()
    }
    let model = movies[indexPath.item]
    cell.configure(model)
    return cell
  }
  
  
}
