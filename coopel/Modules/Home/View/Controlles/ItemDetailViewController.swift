//
//  ItemDetailViewController.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import UIKit

fileprivate enum Constants {
  static let favoriteImageName = "star.fill",
             unfavoriteImageName = "star",
             backgrounColor: UIColor = .systemBackground
}

class ItemDetailViewController: UIViewController {
  
  fileprivate let presenter: ItemDetailPresenter!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupNav()
  }
  
  init(presenter: ItemDetailPresenter) {
      self.presenter = presenter
      super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
      fatalError("init(coder:) is not supported")
  }
  
  private func setupUI(){
    view.backgroundColor = Constants.backgrounColor
    let mainView = MovieCollectionViewCell()
    view.addSubview(mainView)
    mainView.edges(to: view.safeAreaLayoutGuide)
    mainView.configure(presenter.model)
  }
  private func setupNav(){
    let image = UIImage(systemName: Constants.unfavoriteImageName)
    navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(favTapped))
    checkFav()
  }
  
  func checkFav(){
    navigationItem.rightBarButtonItem?.image =  UIImage(systemName: presenter.isFavorite ? Constants.favoriteImageName : Constants.unfavoriteImageName)
  }
  
  @objc func favTapped(sender: Any?){
    presenter.togglefavorite()
    checkFav()
  }
  
}
