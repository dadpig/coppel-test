//
//  MovieCollectionViewCell.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
  
  lazy var coverImageView: UIImageView = {
    let view = UIImageView()
    view.layer.cornerRadius = 8
    view.layer.masksToBounds = true
    return view
  }()
  lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .systemGreen
    return label
  }()
  lazy var overviewLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .justified
    label.numberOfLines = 4
    return label
  }()
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setupUI()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupUI()
  }
  
  override func prepareForReuse(){
    super.prepareForReuse()
    coverImageView.image = nil
  }
  
  private func setupUI(){
    backgroundColor = .systemGray3
    layer.cornerRadius = 8
    layer.masksToBounds = true
    VStack(views: [coverImageView, titleLabel, overviewLabel])
  }
  
  func configure(_ model: MovieCellModel){
    coverImageView.loadFrom(path: model.image)
    titleLabel.text = model.title
    overviewLabel.text = model.overview
  }
}
