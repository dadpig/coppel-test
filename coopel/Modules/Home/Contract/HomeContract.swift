//
//  HomeContract.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation

protocol HomeViewable: AnyObject {
  func showProfile()
  func gotoLogin()
  func success(models: [MovieCellModel], at index: Int)
}
