//
//  ItemDetailPresenter.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation


struct ItemDetailPresenter {
  let model: MovieCellModel!
  var isFavorite: Bool{
    UserDefaults.isFavorite(model.id)
  }
  
  init(model: MovieCellModel){
    self.model = model
  }
  
  func togglefavorite(){
    UserDefaults.toggleFavorite(model.id)
  }
  
}
