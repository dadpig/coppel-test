//
//  HomePresenter.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation


struct HomePresenter {
  weak var view: HomeViewable?
  let movieService: MovieServiceContract!
  let tvService: TVServiceContract!
  
  enum ListItem: String {
    case popular  = "Popular"
    case top = "Top Reted"
    case tv = "On TV"
    case airing = "Airing Today"
  }
  let segmentitems: [HomePresenter.ListItem] = [.popular, .top, .tv, .airing]
  func item(at index: Int) -> ListItem {
    segmentitems[index]
  }
  
  init(movieService: MovieServiceContract, tvService: TVServiceContract,view: HomeViewable){
    self.movieService = movieService
    self.tvService = tvService
    self.view = view
  }
  
  func fetchMovies(by index: Int){
    guard segmentitems.indices.contains(index) else {return}
    let item = segmentitems[index]
    switch item {
    case .popular:
      movieService.popular(page: 1) { result in
        if case .success(let movies) = result {
          view?.success(models: movies.map{.init(movie: $0)}, at: index)
        }
      }
    case .top:
      movieService.topRated(page: 1) { result in
        if case .success(let movies) = result {
          view?.success(models: movies.map{.init(movie: $0)}, at: index)
        }
      }
    case .tv:
      tvService.onAir(page: 1) { result in
        if case .success(let tvs) = result {
          view?.success(models: tvs.map{.init(tv: $0)}, at: index)
        }
      }
    case .airing:
      tvService.airingToday(page: 1) { result in
        if case .success(let tvs) = result {
          view?.success(models: tvs.map{.init(tv: $0)}, at: index)
        }
      }
    }

  }
  func didSelectProfile(){
    view?.showProfile()
  }
  
  func didSelectlogOut(){
    UserDefaults.loginRequestToken = ""
    view?.gotoLogin()
  }
}
