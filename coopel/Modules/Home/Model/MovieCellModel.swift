//
//  MovieCellModel.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation

struct MovieCellModel{
  let id: Int
  let title: String
  let overview: String
  let image: String
  let date: String
  let vote: Double
}


extension MovieCellModel{
  init(movie: Movie) {
    self.init(id: movie.id, title: movie.originalTitle, overview: movie.overview, image: movie.posterPath, date: movie.releaseDate, vote: movie.voteAverage)
  }
  
  init(tv: TV) {
    self.init(id: tv.id, title: tv.name, overview: tv.overview, image: tv.posterPath, date: tv.firstAirDate, vote: tv.voteAverage)
  }
  
}
