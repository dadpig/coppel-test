//
//  ProfileViewController.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import UIKit

//TODO: move to other file

fileprivate enum ProfileConstanst{
  static let title = "Profile",
             backrodColor: UIColor = .systemOrange,
             backrodPath = "https://wallpaperbat.com/img/291060-charmander-iphone-wallpaper-top-free-charmander-iphone.jpg",
             favoriteTitle = "Favorite Shows",
             avatarDimention: CGFloat = 100,
             spasing: CGFloat = 16
}

fileprivate typealias Constants = ProfileConstanst

class ProfileViewController: UIViewController {
  
  fileprivate var models = [MovieCellModel]()
  private lazy var presenter = ProfilePresenter(service: AccountService(), view: self)
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupNav()
    satrtServices()
  }
  lazy var backgroundView: UIImageView = {
    let view = UIImageView()
    view.backgroundColor = Constants.backrodColor
    self.view.addSubview(view)
    return view
  }()
  lazy var avatarImageView: UIImageView = {
    let image = UIImage(systemName: "person.fill")
    let view = UIImageView(image: image)
    view.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      view.heightAnchor.constraint(equalToConstant: Constants.avatarDimention),
      view.widthAnchor.constraint(equalTo: view.heightAnchor)
    ])
    return view
  }()
  lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.text = Constants.title
    return label
  }()
  lazy var nickNameLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    return label
  }()
  lazy var favoritesTitleLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.text = Constants.favoriteTitle
    return label
  }()
  
  lazy var  collectionView: UICollectionView = {
    let collection = UICollectionView(frame: .zero, collectionViewLayout: MoviesLayoutManager.compositionalLayout())
    collection.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: String(describing: MovieCollectionViewCell.self))
    collection.delegate = self
    collection.dataSource = self
    collection.backgroundColor = .clear
    return collection
  }()
  
  private func setupUI(){
    backgroundView.edges(to: view)
    backgroundView.loadFrom(URLAddress: Constants.backrodPath)
    view.VStack(views: [titleLabel, UIView().HStack(views: [avatarImageView, nickNameLabel]), favoritesTitleLabel, collectionView], spacing: Constants.spasing)
  }
  
  private func setupNav(){
    
  }
  
  private func satrtServices(){
    presenter.loadData()
  }
  
}

extension ProfileViewController: ProfileViewable {
  func success(_ accountModel: AccountModel) {
    DispatchQueue.main.async {
      self.nickNameLabel.text = accountModel.nickname
    }
  }
  
  func success(_ favoriteModels: [MovieCellModel]) {
    self.models = favoriteModels
    DispatchQueue.main.async {
      self.collectionView.reloadData()
    }
  }
  
  
}


extension ProfileViewController: UICollectionViewDelegate {
  
}

extension ProfileViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    models.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MovieCollectionViewCell.self), for: indexPath) as? MovieCollectionViewCell
    else  {
      return .init()
    }
    let model = models[indexPath.item]
    cell.configure(model)
    return cell
  }
  
  
}

