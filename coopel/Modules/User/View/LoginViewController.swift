//
//  LoginViewController.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import UIKit

fileprivate enum Constanst {
  static let title = "Login",
             userPlaceholder = "user",
             passwdPlaceholder = "password",
             buttonText = "Login",
             errorMessage = "Error user &/| passwd bla bla",
             spasing: CGFloat = 8,
             fieldsBackgroundColor: UIColor = .systemGray,
             buttonBackgroundColor: UIColor = .systemGray6
}

class LoginViewController: UIViewController {
  //MARK: PRIVAT EPROPERTIES
  lazy var presenter = LoginPresenter(service: AuthService(), view: self)
  
  lazy var backgroundView: UIImageView = {
    let view = UIImageView()
    self.view.addSubview(view)
    return view
  }()
  lazy var containerView: UIView = {
    let view = UIView()
    self.view.addSubview(view)
    return view
  }()
  lazy var userTextField: UITextField = {
    let field = UITextField()
    field.placeholder = Constanst.userPlaceholder
    field.backgroundColor = Constanst.fieldsBackgroundColor
    field.text = "pigdad2"
    return field
  }()
  lazy var passwdTextField: UITextField = {
    let field = UITextField()
    field.placeholder = Constanst.passwdPlaceholder
    field.backgroundColor = Constanst.fieldsBackgroundColor
    field.text = "Pig123"
    return field
  }()
  lazy var buttton: UIButton = {
    let button = UIButton()
    button.setTitle(Constanst.buttonText, for: .normal)
    button.backgroundColor = Constanst.buttonBackgroundColor
    button.addTarget(self, action: #selector(action), for: .touchUpInside)
    return button
  }()
  lazy var errorMessageLabel: UILabel = {
    let label = UILabel()
    label.text = Constanst.errorMessage
    label.textColor = .systemRed
    label.textAlignment = .center
    label.isHidden = true
    view.addSubview(label)
    return label
  }()
  //MARK: LIFE CYCLE
  override func viewDidLoad() {
    super.viewDidLoad()
    UserDefaults.sessionId = ""
    setupUI()
  }
  //MARK: PRIVATE PROPERTIES
  private func setupUI(){
    title = Constanst.title
    backgroundView.edges(to: view)
    containerView.center(to: view)
    containerView.VStack(views: [userTextField, passwdTextField, buttton], spacing: Constanst.spasing)
    errorMessageLabel.edges(to: view, topAnchor: containerView.bottomAnchor)
    backgroundView.loadFrom(URLAddress: GlobalConstants.backgroundAddress)
  }
  @objc func action(sender: Any?){
    guard let user = userTextField.text, let passwd = passwdTextField.text else {return}
    //TODO Validate aftet try to login
    presenter.didTapAction(user: user, passwd: passwd)
  }
}

extension LoginViewController: LoginViewable {
  
  func showError(message: String) {
    DispatchQueue.main.async {
      self.errorMessageLabel.text = message
      self.errorMessageLabel.isHidden = false
    }
  }
  
  func successLogin() {
    DispatchQueue.main.async {
      guard let scene = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else {return}
      scene.rootNav?.setViewControllers([HomeViewController()], animated: true)
    }
  }
  
}
