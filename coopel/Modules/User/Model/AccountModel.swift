//
//  AccountModel.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 04/02/22.
//

import Foundation


struct AccountModel {
  let id: Int
  let nickname: String
}

extension AccountModel {
  init(_ account: Account){
    self.init(id: account.id, nickname: account.username)
  }
}
