//
//  ProfilePresenter.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation

struct ProfilePresenter {
  weak var view: ProfileViewable?
  let service: AccountServiceContract!
  
  init(service: AccountServiceContract, view: ProfileViewable){
    self.service = service
    self.view = view
  }
  
  private func loadAccount(){
    service.detail { result in
      if case .success(let response) = result {
        view?.success(.init(response))
        self.loadFavorites(response.id)
      }
    }
  }
  
  private func loadFavorites(_ accountId: Int){
    service.favorites(accountId: accountId) { result in
      if case .success(let response) = result {
        view?.success(response.map{.init(movie: $0)})
      }
    }
  }
  
  func loadData(){
    if let sessionId = UserDefaults.sessionId, !sessionId.isEmpty {
      loadAccount()
    } else {
      AuthService.createSession { result in
        if case .success = result {
          loadAccount()
        }
      }
    }
    
  }
  
}
