//
//  LoginPresenter.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation

struct LoginPresenter {
  weak var view: LoginViewable?
  let service: AuthServiceContract!
  
  init(service: AuthServiceContract, view: LoginViewable){
    self.service = service
    self.view = view
  }
  
  func didTapAction(user: String, passwd: String){
    service.login(user: user, passwd: passwd){ result in
      if case .success = result {
        view?.successLogin()
      }else {
        view?.showError(message: "Incorrecty")
      }
    }
  }
}
