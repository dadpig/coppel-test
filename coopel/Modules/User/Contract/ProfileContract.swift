//
//  ProfileContract.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation

protocol ProfileViewable: AnyObject {
  func success(_ accountModel: AccountModel)
  func success(_ favoriteModels: [MovieCellModel])
}
