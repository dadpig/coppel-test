//
//  File.swift
//  coopel
//
//  Created by Erick  Cienfuegos on 03/02/22.
//

import Foundation

protocol LoginViewable: AnyObject {
  func showError(message: String)
  func successLogin()
}

